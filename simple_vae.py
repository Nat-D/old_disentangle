# two models sharing a latent variable
# try weight norm or spectral norm
# try to get svhn to have nice generation first
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten


def wn_conv(x, scope, *, nf, rf, stride, pad='VALID', init_scale=1.0):
    with tf.variable_scope(scope):
        nin = x.get_shape()[3].value
        v = tf.get_variable("w", [rf, rf, nin, nf], initializer=ortho_init(init_scale))
        b = tf.get_variable("b", [nf], initializer=tf.constant_initializer(0.0))
        g = tf.get_variable("g", [nf], initializer=tf.truncated_normal_initializer(1.0))

    w = tf.reshape(g,[1,1,1,nf])*tf.nn.l2_normalize(v,[0,1,2])
    x = tf.nn.bias_add(tf.nn.conv2d(x, w, [1, stride, stride, 1], pad), b)
    return x



def wn_fc(x, scope, nh):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        g = tf.get_variable("g", [nh], initializer=tf.truncated_normal_initializer(1.0))
        v = tf.get_variable("v", [nin, nh], initializer=ortho_init(1.0))
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(0.0))

    # use weight normalization (Salimans & Kingma, 2016)
    x = tf.matmul(x, v)
    scaler = g/tf.sqrt(tf.reduce_sum(tf.square(v),[0]))
    # x = wx +b  = g v/|v| x + b
    x = tf.reshape(scaler,[1,nh])*x + tf.reshape(b,[1,nh])
    return x


class VAE:
    def __init__(self, sess, x):
        self.sess = sess

        # build autoencoder
        z_mean, z_sig = self._build_encoder(x)
        eps = tf.random_normal(tf.shape(z_sig))
        self.z_sample = z_sample = z_mean + eps * z_sig
        x_recon = self._build_generator(z_sample)
        x_mean = x_recon[:,:,:,:3]
        x_logscale = x_recon[:,:,:,3:]

        # KL loss functions E_q[log Q - log P]
        kl_loss = tf.reduce_mean(
                    - gaussian_log_likelihood(z_sample, 0.0, 1.0, eps=0.0) \
                    + gaussian_log_likelihood(z_sample, z_mean, z_sig)
                  )

        # recon loss function
        recon_loss =tf.reduce_mean(
                    tf.reduce_sum(
                    discretised_logistic_loss(x, x_mean, x_logscale),axis=[1,2,3]))

        total_loss = recon_loss + kl_loss
        optimizer  = tf.train.AdamOptimizer(0.0001)
        self.train_op = optimizer.minimize(total_loss)

        """
        SUMMARY
        """
        self.summary = tf.summary.merge([
            tf.summary.scalar("train/recon_loss", recon_loss),
            tf.summary.scalar("train/kl_loss", kl_loss),
            tf.summary.scalar("train/total_loss", total_loss),
            tf.summary.image("x", 0.5*(x+1.)),
            tf.summary.image("x_recon", 0.5*(x_mean[:,:,:,:3] + 1.))])
        self.vis_placeholder = tf.placeholder(tf.uint8)
        self.random_gen_summary = self._build_random_generator()

    def _build_random_generator(self):
        # random generator

        def gen_x_sample(z):
            x_gen = self._build_generator(z, reuse=True)
            x_gen_mean = x_gen[:,:,:,:3]
            x_gen_logscale = x_gen[:,:,:,3:]
            x_gen_samples = samples_logistic_distribution(x_gen_mean, x_gen_logscale)
            #x_gen_samples = x_gen_mean

            x_gen_samples = tf.reshape(x_gen_samples, [1, 100*32,32,3])
            x_gen_samples = tf.split(x_gen_samples, 10, axis=1)
            x_gen_samples = tf.concat(x_gen_samples, axis=2)
            return x_gen_samples
        #
        z_random = tf.random_normal([100, self.zsize])
        x_gen_samples = gen_x_sample(z_random)

        return tf.summary.merge(
                    [tf.summary.image('random_gen/z1', x_gen_samples)]
                    )

    def _build_interpolation(self):

        return

    def _build_encoder(self, x, reuse=False):
        with tf.variable_scope('encoder', reuse=reuse):

            h = wn_conv(x, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = flatten(h)
            h = wn_fc(h, 'fc1', 200)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            z = wn_fc(h, 'fc2', 200)

            z_mean = z[:, :100]
            z_sig  = tf.nn.softplus( z[:, 100:] )

            self.zsize = z_mean.get_shape()[1].value
        return z_mean, z_sig

    def _build_generator(self, z, reuse=False):
        with tf.variable_scope('decoder', reuse=reuse):
            # try autoregressive prior

            h = wn_fc(z, 'h1', 200)
            h = tf.reshape(h, [-1, 1, 1, 200])
            h = tf.image.resize_images(h, [4,4], align_corners=True)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv1', nf=128, rf=4, stride=1, pad='SAME')
            h = tf.image.resize_images(h, [8,8], align_corners=True)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=4, stride=1, pad='SAME')
            h = tf.image.resize_images(h, [16,16], align_corners=True)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv3', nf=32, rf=6, stride=1, pad='SAME')
            h = tf.image.resize_images(h, [32,32], align_corners=True)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            x = wn_conv(h, 'conv4', nf=6, rf=6, stride=1, pad='SAME')

        return x

    def train(self, summary=True):
        if summary:
            return self.sess.run([self.summary, self.train_op])
        else:
            return self.sess.run([self.train_op])

    def random_generation(self):
        return self.sess.run(self.random_gen_summary)

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--logdir', help='tb directory', default='experiments/')
    parser.add_argument('--exp_name', default='test')
    args = parser.parse_args()

    summary_dir = args.logdir + args.exp_name
    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:
        summary_writer = tf.summary.FileWriter(summary_dir)

        data = SVHN(sess, train_batch_size=250)
        x = data.next_train_images
        net = VAE(sess, x)
        sess.run(tf.global_variables_initializer())

        for i in range(100000000):
            if i % 100 == 0:
                summary, _ = net.train()
                random_gen_summary = net.random_generation()
                summary_writer.add_summary(summary, i)
                summary_writer.add_summary(random_gen_summary, i)
                summary_writer.flush()
            else:
                _ = net.train(False)
