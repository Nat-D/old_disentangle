import numpy as np
import tensorflow as tf


def ortho_init(scale=1.0):
    def _ortho_init(shape, dtype, partition_info=None):
        #lasagne ortho init for tf
        shape = tuple(shape)
        if len(shape) == 2:
            flat_shape = shape
        elif len(shape) == 4: # assumes NHWC
            flat_shape = (np.prod(shape[:-1]), shape[-1])
        else:
            raise NotImplementedError
        a = np.random.normal(0.0, 1.0, flat_shape)
        u, _, v = np.linalg.svd(a, full_matrices=False)
        q = u if u.shape == flat_shape else v # pick the one with the correct shape
        q = q.reshape(shape)
        return (scale * q[:shape[0], :shape[1]]).astype(np.float32)
    return _ortho_init


def fc(x, scope, nh, *, init_scale=1.0, init_bias=0.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nh], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(init_bias), trainable=trainable)
        return tf.matmul(x, w)+b

def conv(x, scope, *, nf, rf, stride, pad='VALID', init_scale=1.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[3].value
        w = tf.get_variable("w", [rf, rf, nin, nf], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nf], initializer=tf.constant_initializer(0.0), trainable=trainable)
        return tf.nn.conv2d(x, w, strides=[1, stride, stride, 1], padding=pad)+b



def wn_conv(x, scope, *, nf, rf, stride, pad='VALID', init_scale=1.0):
    with tf.variable_scope(scope):
        nin = x.get_shape()[3].value
        v = tf.get_variable("w", [rf, rf, nin, nf], initializer=ortho_init(init_scale))
        b = tf.get_variable("b", [nf], initializer=tf.constant_initializer(0.0))
        #g = tf.get_variable("g", [nf], initializer=tf.truncated_normal_initializer(0.1))
        g = tf.get_variable("g", [nf], initializer=tf.truncated_normal_initializer(1.0))


    w = tf.reshape(g,[1,1,1,nf])*tf.nn.l2_normalize(v,[0,1,2])
    x = tf.nn.bias_add(tf.nn.conv2d(x, w, [1, stride, stride, 1], pad), b)
    return x

def wn_fc(x, scope, nh, init_scale=1.0):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        #g = tf.get_variable("g", [nh], initializer=tf.truncated_normal_initializer(0.1))
        g = tf.get_variable("g", [nh], initializer=tf.truncated_normal_initializer(init_scale))
        v = tf.get_variable("v", [nin, nh], initializer=ortho_init(1.0))
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(0.0))

    # use weight normalization (Salimans & Kingma, 2016)
    x = tf.matmul(x, v)
    scaler = g/tf.sqrt(tf.reduce_sum(tf.square(v),[0]))
    # x = wx +b  = g v/|v| x + b
    x = tf.reshape(scaler,[1,nh])*x + tf.reshape(b,[1,nh])
    return x


def gumbel_argmax(logits):
    noise = tf.random_uniform(tf.shape(logits))
    nz = tf.shape(logits)[1]
    return tf.one_hot( tf.argmax(logits - tf.log(-tf.log(noise)), 1), nz)

def softmax(logits, tau, axis):
    x = logits/tau
    b = tf.reduce_max(x, axis=axis, keepdims=True)
    exp_logits = tf.exp(x-b)
    return exp_logits / tf.reduce_sum(exp_logits, axis, keepdims=True)

def gumbel_softmax(logits, tau):
    noise = tf.random_uniform(tf.shape(logits))
    return softmax(logits - tf.log(-tf.log(noise)), tau, axis=1)

def cat_entropy(logits):
    a0 = logits - tf.reduce_max(logits, 1, keepdims=True)
    ea0 = tf.exp(a0)
    z0 = tf.reduce_sum(ea0, 1, keepdims=True)
    p0 = ea0 / z0
    return tf.reduce_sum(p0 * (tf.log(z0) - a0), 1)

def flatten(x):
    return tf.reshape(x, [-1, np.prod(x.get_shape().as_list()[1:])])

def discretised_logistic_loss(x, m, log_scales):
    # modified from https://github.com/openai/pixel-cnn/blob/master/pixel_cnn_pp/nn.py
    centered_x = x - m
    inv_stdv = tf.exp(-log_scales)
    plus_in = inv_stdv * (centered_x + 1./255.)
    min_in = inv_stdv * (centered_x - 1./255.)
    cdf_plus = tf.nn.sigmoid(plus_in) # 1/(exp(-x)+1)
    cdf_min = tf.nn.sigmoid(min_in)
    cdf_delta = cdf_plus - cdf_min

    mid_in = inv_stdv * centered_x
    log_pdf_mid = mid_in - log_scales - 2.*tf.nn.softplus(mid_in)

    log_cdf_plus = plus_in - tf.nn.softplus(plus_in)
    log_one_minus_cdf_min = -tf.nn.softplus(min_in) # - log(exp(x)+ 1)

    log_prob = tf.where(x < -0.999, log_cdf_plus, tf.where(x > 0.999, log_one_minus_cdf_min, tf.where(cdf_delta > 1e-5, tf.log(tf.maximum(cdf_delta, 1e-12)), log_pdf_mid - np.log(127.5))))
    return - log_prob


def discretised_logistic_loss_v2(sample, mean, logscale, binsize=1 / 256.0):
    # from https://github.com/openai/iaf/blob/7331a3037849007cd7a097e044b68596e621814d/tf_utils/distributions.py
    scale = tf.exp(logscale)
    sample = (tf.floor(sample / binsize) * binsize - mean) / scale
    logp = tf.log(tf.sigmoid(sample + binsize / scale) - tf.sigmoid(sample) + 1e-7)
    return - logp


def samples_logistic_distribution(means, log_scales):
    u = tf.random_uniform(tf.shape(means), minval=1e-5, maxval=1. - 1e-5)
    x = means + tf.exp(log_scales)*(tf.log(u) - tf.log(1. - u))
    x = tf.minimum(tf.maximum(x, -1.), 1.) #clip
    return x

def compute_pairwise_distances(x, y):
    norm = lambda x: tf.reduce_sum(tf.square(x), 1)
    return norm(tf.expand_dims(x, 2) - tf.expand_dims(tf.transpose(y),0) )

def RBF_kernel(dist, var=1.0):
    return tf.exp(-0.5*dist/var)

def Dirac_kernel(x,y):
    return tf.reduce_sum(
            tf.multiply( tf.expand_dims(x, 2),  tf.expand_dims(tf.transpose(y),0))
            ,axis=1)

def compute_RBF_kernels(z_sample, z_prior):
    dist_ss = compute_pairwise_distances(z_sample, z_sample)
    dist_pp = compute_pairwise_distances(z_prior, z_prior)
    dist_sp = compute_pairwise_distances(z_sample, z_prior)

    n = tf.shape(z_sample)[0]
    half_size = tf.cast( (n * n) / 2, tf.int32)
    # median heuristic
    var = tf.nn.top_k(
            input=tf.reshape(dist_sp, [-1]), k=half_size).values[half_size - 1]

    k_ss = RBF_kernel(dist_ss, var)
    k_pp = RBF_kernel(dist_pp, var)
    k_sp = RBF_kernel(dist_sp, var)
    return k_ss, k_pp, k_sp

def compute_mmd(z_sample, z_prior):

    k_ss, k_pp, k_sp = compute_RBF_kernels(z_sample, z_prior)

    k_ss = tf.reduce_sum(k_ss)
    k_pp = tf.reduce_sum(k_pp)
    k_sp = tf.reduce_sum(k_sp)

    bs = tf.shape(z_sample)[0]
    n = tf.cast(bs, tf.float32)
    nn_1 = n*(n-1.)

    return  k_pp/nn_1 + k_ss/nn_1 - 2.0 * k_sp/(n*n)


from scipy import linalg as LA
def PCA(data, dims_rescaled_data=2):
    """
    returns: data transformed in 2 dims/columns + regenerated original data
    pass in: data as 2D NumPy array
    """
    m, n = data.shape
    # mean center the data
    data -= data.mean(axis=0)
    # calculate the covariance matrix
    R = np.cov(data, rowvar=False)
    # calculate eigenvectors & eigenvalues of the covariance matrix
    # use 'eigh' rather than 'eig' since R is symmetric,
    # the performance gain is substantial
    evals, evecs = LA.eigh(R)
    # sort eigenvalue in decreasing order
    idx = np.argsort(evals)[::-1]
    evecs = evecs[:,idx]
    # sort eigenvectors according to same index
    evals = evals[idx]
    # select the first n eigenvectors (n is desired dimension
    # of rescaled data array, or dims_rescaled_data)
    evecs = evecs[:, :dims_rescaled_data]
    # carry out the transformation on the data using eigenvectors
    # and return the re-scaled data, eigenvalues, and eigenvectors
    return np.dot(evecs.T, data.T).T, evals, evecs


def gaussian_log_likelihood(x, mean, sig, eps=1e-8):
    # compute log P(x) for diagonal Guassian
    # -1/2 log( (2pi)^k sig_1 * sig_2 * ... * sig_k ) -  sum_i 1/2sig_i^2 (x_i - m_i)^2
    return -0.5 * tf.reduce_sum( tf.log(2.*np.pi*sig*sig + eps)
                               + tf.square(x-mean)/(sig*sig + eps), axis=1)

def gaussian_entropy(mean, sig, eps=1e-8):
    # 1/2 log(2 pi e sig sig)
    return 0.5 * tf.reduce_sum( tf.log(2.*np.pi*np.e*sig*sig + eps), axis=1)

# analytic KL
def KL(mean, sig):
    log_var = tf.log(tf.square(sig) + 1.e-8)
    return - 0.5 * tf.reduce_sum( 1 + log_var - tf.square(mean) - tf.exp(log_var), axis=1)

def planar_flow(z, scope, init_scale=1.0, init_bias=0.0):
    with tf.variable_scope(scope):
        nin = z.get_shape()[1].value
        w = tf.get_variable("w", [nin, 1], initializer=ortho_init(init_scale))
        u = tf.get_variable("u", [nin, 1], initializer=ortho_init(init_scale))
        b = tf.get_variable("b", [1], initializer=tf.constant_initializer(init_bias))
    # code snippet from https://github.com/AMLab-Amsterdam/MNF_VBNN/blob/master/norm_flows.py
    uw = tf.reduce_sum(u * w)
    muw = -1 + tf.nn.softplus(uw)  # = -1 + T.log(1 + T.exp(uw))
    u_hat = u + (muw - uw) * w / tf.reduce_sum(w ** 2)
    zwb = tf.matmul(z, w) + b
    psi = tf.matmul(1 - tf.nn.tanh(zwb) ** 2, tf.transpose(w))  # tanh(x)dx = 1 - tanh(x)**2
    psi_u = tf.matmul(psi, u_hat)

    logdets = tf.squeeze(tf.log(tf.abs(1 + psi_u)))
    z = z + tf.matmul(tf.nn.tanh(zwb), tf.transpose(u_hat))

    return z, logdets

def masked_fc(x, scope, *, inclusive=True, init_scale=1.0, init_bias=0.0):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nin], initializer=ortho_init(init_scale))
        b = tf.get_variable("b", [nin], initializer=tf.constant_initializer(init_bias))

        if inclusive:
            mask = np.triu(np.ones([nin, nin]))
        else:
            mask = np.triu(np.ones([nin, nin])) - np.eye(nin)
        m = tf.constant(mask, tf.float32)
        mw = tf.multiply(m, w)
        return tf.matmul(x, mw)+b

def IAF(z, h, scope, init_scale=1.0, init_bias=0.0):
    # 1.) transform z -> mu and sig with masked autoencoder
    # 2.) shifted and scale z' = z*sig + mu
    with tf.variable_scope(scope):
        act = tf.nn.relu
        ln = tf.contrib.layers.layer_norm

        h1 = act(ln(masked_fc(z, 'h1_z', inclusive=False)))
        h1 = h1 + h
        h1 = act(ln(masked_fc(h1, 'h2')))

        mu = masked_fc(h1, 'mu')
        sig = tf.nn.softplus( masked_fc(h1, 'sig') )
        z_out = z * sig + mu
        logdet = tf.reduce_sum( tf.log(sig + 1e-8), axis=1)

        # shuffle
        hsize = (z.get_shape())[1]
        init_perm = np.random.permutation(int(hsize))
        perm = tf.get_variable('perm', initializer=init_perm, trainable=False)
        z_out = tf.gather(z_out, perm, axis=1)

    return z_out, logdet
