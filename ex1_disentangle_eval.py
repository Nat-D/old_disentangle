# 1 load vae model
# 2 encode a test sample
# 3 decode 100 samples with randomize local code
# 4 classify the samples and measure consistency n/100
import tensorflow as tf
import numpy as np

from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten,\
                  gumbel_softmax, gumbel_argmax

def wn_fc(x, scope, nh, *, init_scale=1.0, init_bias=0.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nh], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(init_bias), trainable=trainable)
        return tf.matmul(x, w)+b

def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)
    return _initializer

def wn_conv(x, scope, nf, rf, stride, pad="SAME", init_scale=1.0, dtype=tf.float32, collections=None):
    with tf.variable_scope(scope):
        stride_shape = [1, stride, stride, 1]
        filter_shape = [rf, rf, int(x.get_shape()[3]), nf]

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = np.prod(filter_shape[:3])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = np.prod(filter_shape[:2]) * nf
        # initialize weights with random weights
        w_bound = np.sqrt(6. / (fan_in + fan_out))

        w = tf.get_variable("W", filter_shape, dtype, tf.random_uniform_initializer(-w_bound, w_bound),
                            collections=collections)
        b = tf.get_variable("b", [1, 1, 1, nf], initializer=tf.constant_initializer(0.0),
                            collections=collections)

        return tf.nn.conv2d(x, w, stride_shape, pad) + b


class VAE:
    def __init__(self, sess, x):
        self.sess = sess

        xz = x[:,:,:, :3]
        xw = x[:,:,:, 3:]

        # build autoencoder
        zg_mean, zg_sig, zl_mean, zl_sig  = self._build_encoder(x)

        # set zl_sig = 1
        # set zl_mean = 0
        zl_mean = zl_mean
        zl_sig = zl_sig

        z_mean = tf.concat([zg_mean, zl_mean], axis=1)
        z_sig  = tf.concat([zg_sig, zl_sig], axis=1)

        z_mean = tf.tile(z_mean, [1, 1])
        z_sig = tf.tile(z_sig, [1, 1])

        eps = tf.random_normal(tf.shape(z_sig))
        self.z_sample = z_sample = z_mean + eps * z_sig
        self.zsize = z_sample.get_shape()[1].value

        x_recon = self._build_generator(z_sample, reuse=False, name='z_decoder')
        x_mean = x_recon[:,:,:,:3]
        x_log_scale = x_recon[:,:,:,3:]
        self.x_outs = samples_logistic_distribution(x_mean, x_log_scale)
        #self.x_outs = x_recon[:,:,:,:3]

    def _build_encoder(self, x, reuse=False):
        #with tf.variable_scope('model', reuse=reuse):
            #with tf.variable_scope('encoder', reuse=reuse):
        xg = x[:,:,:, :3]
        xl = x[:,:,:, 3:]

        zg_mean, zg_sig   = self._build_encoder_g(xg, reuse)
        zl_mean, zl_sig   = self._build_encoder_l(xl, reuse)

        return zg_mean, zg_sig, zl_mean, zl_sig

    def _build_encoder_g(self, xg, reuse):
        with tf.variable_scope('xg', reuse=reuse):
            h = wn_conv(xg, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            zg_mean, zg_sig = self._build_z_block(h, 'zg', reuse=reuse)

        return zg_mean, zg_sig

    def _build_encoder_l(self, xl, reuse):
        with tf.variable_scope('xl', reuse=reuse):
            h = wn_conv(xl, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            zl_mean, zl_sig = self._build_z_block(h, 'zl', reuse=reuse)
        return zl_mean, zl_sig


    def _build_z_block(self, h, name, reuse):
        with tf.variable_scope(name, reuse=reuse):

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            z_mean = wn_fc(h, 'fc_z', 100)
            z_sig  = tf.nn.sigmoid( wn_fc(h, 'fc_z_sig', 100, init_scale=1.0))
        return z_mean, z_sig


    def _build_generator(self, z, reuse=False, name='decoder'):
        with tf.variable_scope('model'):
            with tf.variable_scope(name, reuse=reuse):
                h = wn_fc(z, 'h1', 200)
                h = tf.reshape(h, [-1, 1, 1, 200])
                h = tf.image.resize_images(h, [4,4], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv1', nf=128, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [8,8], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv2', nf=64, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [16,16], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv3', nf=32, rf=6, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [32,32], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                x = wn_conv(h, 'conv4', nf=6, rf=6, stride=1, pad='SAME')

        return x

class Classifier:
    def __init__(self, sess, x):
        self.sess = sess

        self.y_pred = self._build_classifier(x, reuse=False)

    def _build_classifier(self, x, reuse):
        with tf.variable_scope('classifier', reuse=reuse):
            h = wn_conv(x, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv4', nf=248, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_fc(h, 'fc2', 200)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            y_logits = wn_fc(h, 'y_logits', 10)
            y_pred = tf.argmax(y_logits, axis=1)

        return y_pred


if __name__ == "__main__":

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:
        data = SVHN_AUG(sess, train_batch_size=64, test_batch_size=1000)
        x = data.next_test_images
        label = data.next_test_labels
        vae_net = VAE(sess, x)

        saver1 = tf.train.Saver()
        saver1.restore(sess, 'models/test/at_2m/model.ckpt')

        classifier = Classifier(sess, vae_net.x_outs)
        saver2 = tf.train.Saver([v for v in tf.global_variables() if 'classifier' in v.name])
        saver2.restore(sess, 'models/classifier_3/model.ckpt')

        y_pred, y  = sess.run([classifier.y_pred, tf.argmax(label, axis=1)])

        acc = tf.reduce_mean( tf.cast( tf.equal(classifier.y_pred,  tf.argmax(label, axis=1)) , tf.float32) )
        #print(y)
        #print(y_pred)
        print(sess.run(acc))

        #gen = sess.run(vae_net.x_outs)
        #print(np.shape(gen))
