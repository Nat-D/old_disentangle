# Evaluate clustering acc of trained model on test set
import tensorflow as tf
import numpy as np
from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten,\
                  gumbel_softmax, gumbel_argmax, wn_conv, wn_fc

import matplotlib.pyplot as plt

def wn_fc(x, scope, nh, *, init_scale=1.0, init_bias=0.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nh], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(init_bias), trainable=trainable)
        return tf.matmul(x, w)+b

def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)
    return _initializer

def wn_conv(x, scope, nf, rf, stride, pad="SAME", init_scale=1.0, dtype=tf.float32, collections=None):
    with tf.variable_scope(scope):
        stride_shape = [1, stride, stride, 1]
        filter_shape = [rf, rf, int(x.get_shape()[3]), nf]

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = np.prod(filter_shape[:3])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = np.prod(filter_shape[:2]) * nf
        # initialize weights with random weights
        w_bound = np.sqrt(6. / (fan_in + fan_out))

        w = tf.get_variable("W", filter_shape, dtype, tf.random_uniform_initializer(-w_bound, w_bound),
                            collections=collections)
        b = tf.get_variable("b", [1, 1, 1, nf], initializer=tf.constant_initializer(0.0),
                            collections=collections)

        return tf.nn.conv2d(x, w, stride_shape, pad) + b



class Classifier:
    def __init__(self, x, labels, ysize):

        self.ysize = ysize

        py, y_logits = self._build_classifier(x)
        self.acc  = self._build_accuracy(py, labels)
        self.py = py

    def _build_classifier(self, x, reuse=False):
        _, y_logits = self._build_encoder_g(x, reuse=reuse)
        py = tf.nn.softmax(y_logits, axis=1)
        return py, y_logits

    def _build_encoder_g(self, xg, reuse):
        with tf.variable_scope('xg', reuse=reuse):
            h = wn_conv(xg, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

        y, y_logits = self._build_y_block(h, 'y', reuse=reuse)

        return y, y_logits

    def _build_y_block(self, h, name, reuse):
        with tf.variable_scope(name, reuse=reuse):
            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000) # was 500
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_fc(h, 'fc2', 500)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            y_logits = wn_fc(h, 'fc_z', self.ysize)
            y = gumbel_softmax(y_logits, tau=0.4)

        return y, y_logits


    def _build_accuracy(self, py, labels):
        # for each cluster
        # find an example that have highest py and assign the cluster
        # with true label from that example.
        idx = tf.argmax(py, axis=0)
        pred = tf.argmax(py, axis=1)
        labels = tf.argmax(labels, axis=1)
        # create a mapping
        examples = tf.gather(idx, pred)
        new_pred = tf.gather(labels, examples)
        acc = tf.cast( tf.equal(new_pred,labels) , tf.float32)
        bs = tf.shape(acc)[0]
        return tf.reduce_sum(acc), bs

    def _build_acc_with_majority_vote(self, py, labels):
        # for each cluster
        # looks at all the example and count each labels
        # the label that has the most count is assigned to that cluster
        pred = tf.argmax(py, axis=1)
        labels = tf.argmax(labels, axis=1)

        # look at each element of pred and labels
        # keep label in memory





if __name__ == "__main__":

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:

        ysize = 30
        data = SVHN(sess, train_batch_size=1, test_batch_size=10000)

        x = data.next_test_images
        labels = data.next_test_labels

        classifier = Classifier(x, labels, ysize)
        saver = tf.train.Saver()
        saver.restore(sess, 'models/v43_bs128_y30_b30_a1_t0p4_run1/model.ckpt')


        py, lbs = sess.run([classifier.py, labels])
        py_2, lbs_2 = sess.run([classifier.py, labels])
        py_3, lbs_3 = sess.run([classifier.py, labels])

        py = np.concatenate((py, py_2, py_3), axis=0)
        lbs = np.concatenate((lbs, lbs_2, lbs_3), axis=0)
        num_test = np.shape(py)[0]

        preds = np.argmax(py, axis=1)
        lbs  = np.argmax(lbs, axis=1)
        print(preds)
        print(lbs)

        hist = np.zeros([ysize, 10]) # assignment matrix
        for pred, lb in zip(preds, lbs):
                hist[pred, lb] += 1

        # assign label based on majority
        new_idxs = np.argmax(hist, axis=1)
        hist_new = []
        print(new_idxs)

        acc_count = 0

        for i in range(10):
            for j in range(ysize):
                if new_idxs[j] == i :
                    hist_new += [hist[j]]
                    acc_count += hist[j][i]

        print(acc_count)
        print(num_test)
        print(acc_count/num_test)

        hist_new = np.transpose(hist_new)

        plt.yticks(np.arange(0, 10, 2, dtype=np.int))


        ax = plt.gca();
        # Minor ticks
        ax.set_xticks(np.arange(-.5, 30, 1), minor=True);
        ax.set_yticks(np.arange(-.5, 10, 1), minor=True);
        # Gridlines based on minor ticks
        ax.grid(which='minor', color='black', linestyle='-', linewidth=0.5)

        plt.imshow(hist_new, cmap='gray_r')
        plt.show()
