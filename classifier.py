import tensorflow as tf
import numpy as np

from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten,\
                  gumbel_softmax, gumbel_argmax

def wn_fc(x, scope, nh, *, init_scale=1.0, init_bias=0.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nh], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(init_bias), trainable=trainable)
        return tf.matmul(x, w)+b

def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)
    return _initializer

def wn_conv(x, scope, nf, rf, stride, pad="SAME", init_scale=1.0, dtype=tf.float32, collections=None):
    with tf.variable_scope(scope):
        stride_shape = [1, stride, stride, 1]
        filter_shape = [rf, rf, int(x.get_shape()[3]), nf]

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = np.prod(filter_shape[:3])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = np.prod(filter_shape[:2]) * nf
        # initialize weights with random weights
        w_bound = np.sqrt(6. / (fan_in + fan_out))

        w = tf.get_variable("W", filter_shape, dtype, tf.random_uniform_initializer(-w_bound, w_bound),
                            collections=collections)
        b = tf.get_variable("b", [1, 1, 1, nf], initializer=tf.constant_initializer(0.0),
                            collections=collections)

        return tf.nn.conv2d(x, w, stride_shape, pad) + b


class Classifier:
    def __init__(self, sess, x, y, x_eval, y_eval):
        self.sess = sess

        loss, y_pred = self._build_classifier(x, y, reuse=False)


        optimizer  = tf.train.AdamOptimizer(0.0001)
        self.train_op = optimizer.minimize(loss)

        y_labels = tf.argmax(y, axis=1)
        acc =  tf.reduce_mean( tf.cast( tf.equal(y_pred, y_labels) , tf.float32) )

        self.summary = tf.summary.merge([
            tf.summary.scalar("train/loss", loss),
            tf.summary.scalar("train/acc", acc),
            ])

        eval_loss, eval_y_pred = self._build_classifier(x_eval, y_eval, reuse=True)
        eval_y_labels = tf.argmax(y_eval, axis=1)
        eval_acc =  tf.reduce_mean( tf.cast( tf.equal(eval_y_pred, eval_y_labels) , tf.float32) )

        self.eval_summary = tf.summary.merge([
            tf.summary.scalar("eval/loss", eval_loss),
            tf.summary.scalar("eval/acc", eval_acc),
        ])

    def _build_classifier(self, x, y, reuse):
        with tf.variable_scope('classifier', reuse=reuse):
            h = wn_conv(x, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv4', nf=248, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_fc(h, 'fc2', 200)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            y_logits = wn_fc(h, 'y_logits', 10)
            y_pred = tf.argmax(y_logits, axis=1)

            loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=y_logits))

        return loss, y_pred

    def train(self, summary=True):
        if summary:
            return self.sess.run([self.summary, self.train_op])
        else:
            return self.sess.run([self.train_op])

    def eval(self):
        return self.sess.run(self.eval_summary)

if __name__ == "__main__":
    # build a classifier
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--logdir', help='tb directory', default='experiments/')
    parser.add_argument('--modeldir',help='model directory', default='models/')
    parser.add_argument('--exp_name', default='classifier')
    parser.add_argument('--gpu', default="0")
    parser.add_argument('--bs', default="256", type=int)


    args = parser.parse_args()

    summary_dir = args.logdir + args.exp_name
    model_dir = args.modeldir + args.exp_name

    import os
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"]=args.gpu


    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:
        summary_writer = tf.summary.FileWriter(summary_dir)

        data = SVHN(sess, train_batch_size=64, test_batch_size=10000)

        x = data.next_train_images
        labels = data.next_train_labels

        x_test = data.next_test_images
        labels_test = data.next_test_labels

        net = Classifier(sess, x, labels, x_test, labels_test)
        sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()

        for i in range(1000002):

            if (i) % 1000 == 0:
                summary, _ = net.train()
                eval_summary = net.eval()

                summary_writer.add_summary(summary, i)
                summary_writer.add_summary(eval_summary, i)
                summary_writer.flush()
                saver.save(sess, model_dir + '/model.ckpt')

            else:
                _ = net.train(False)
