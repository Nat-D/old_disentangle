# Evaluate clustering acc of trained model on test set
import tensorflow as tf
import numpy as np
from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten,\
                  gumbel_softmax, gumbel_argmax, wn_conv, wn_fc

import matplotlib.pyplot as plt
from PIL import Image

class VAE:
    def __init__(self, sess, x, labels, ysize, beta):
        self.sess = sess
        self.x = x
        self.ysize = ysize

        xz = x[:,:,:, :3]
        xw = x[:,:,:, 3:]

        # build autoencoder
        zg_mean, zg_sig, zl_mean, zl_sig ,\
        y, y_logits = self._build_encoder(x)

        # build a prior network
        zg_mean_prior, zg_sig_prior = self._build_prior(y)
        zg_var       = zg_sig * zg_sig
        zg_var_prior = zg_sig_prior * zg_sig_prior

        # combine bottom-up and top-down inference
        zg_var_combine = zg_var / ( zg_var + zg_var_prior)
        zg_sig_combine = tf.sqrt(zg_var_combine)
        zg_mean_combine = ( zg_var_prior * zg_mean +  zg_var * zg_mean_prior )/ (zg_var_prior + zg_var)

        z_mean = tf.concat([zg_mean_combine, zl_mean], axis=1)
        z_sig  = tf.concat([zg_sig_combine, zl_sig], axis=1)
        eps = tf.random_normal(tf.shape(z_sig))
        self.z_sample = z_sample = z_mean + eps * z_sig
        self.zsize = z_sample.get_shape()[1].value


        self.x_recon = x_recon = self._build_generator(z_sample, reuse=False, name='z_decoder')
        x_mean = x_recon[:,:,:,:3]
        x_logscale = x_recon[:,:,:,3:]


        # KL-cost for y: E_q[ log q - log p] = sum_i pi_i log pi_i  - log1/k
        py = tf.nn.softmax(y_logits, axis=1)
        kl_loss_y = tf.reduce_mean(
                    tf.reduce_sum(py * tf.log(py + 1e-8) ,axis=1) - np.log(1.0/self.ysize))

        # KL loss functions E_q[log Q - log P]
        z_sample_g, z_sample_l = tf.split(z_sample, 2, axis=1)

        kl_loss_g = tf.reduce_mean(
                    - gaussian_log_likelihood(z_sample_g, zg_mean_prior, zg_sig_prior) \
                    + gaussian_log_likelihood(z_sample_g, zg_mean_combine, zg_sig_combine)
                    )

        kl_loss_l = tf.reduce_mean(
                    - gaussian_log_likelihood(z_sample_l, 0.0, 1.0, eps=0.0) \
                    + gaussian_log_likelihood(z_sample_l, zl_mean, zl_sig)
                    )

        kl_loss = kl_loss_g + kl_loss_l
        # recon loss function
        recon_loss =tf.reduce_mean(
                    tf.reduce_sum(
                    discretised_logistic_loss(xz, x_mean, x_logscale),axis=[1,2,3]))


        # Auxiliary model
        self.xw_recon = xw_recon = self._build_generator(z_sample_l, reuse=False, name='w_decoder')
        xw_mean = xw_recon[:,:,:,:3]
        xw_logscale = xw_recon[:,:,:,3:]

        recon_loss_w =tf.reduce_mean(
                    tf.reduce_sum(
                    discretised_logistic_loss(xw, xw_mean, xw_logscale),axis=[1,2,3]))


        # optimise  beta, alpha
        total_loss = (recon_loss) + (recon_loss_w ) + (beta * 1.0) * kl_loss + 1.0 * kl_loss_y

        optimizer  = tf.train.AdamOptimizer(0.0001)
        self.train_op = optimizer.minimize(total_loss)

        """
        train/accuracy
        """
        py = tf.nn.softmax(y_logits, axis=1)
        acc  = self._build_accuracy(py, labels)

        """
        SUMMARY
        """
        self.summary = tf.summary.merge([
            tf.summary.scalar("train/recon_loss", recon_loss),
            tf.summary.scalar("train/kl_loss", kl_loss),
            tf.summary.scalar("train/recon_loss_w", recon_loss_w),
            tf.summary.scalar("train/kl_y", kl_loss_y),
            tf.summary.scalar("train/total_loss", total_loss),
            tf.summary.scalar("train/clustering_acc", acc),
            tf.summary.image("x/x", 0.5*(xz+1.)),
            tf.summary.image("w/x_shuffle", 0.5*(xw+1.)),
            tf.summary.image("x/recon", 0.5*(x_mean[:,:,:,:3] + 1.)),
            tf.summary.image("w/recon", 0.5*(xw_mean[:,:,:,:3] + 1.)),
            ])

        self.vis_placeholder = tf.placeholder(tf.uint8)
        self.random_images = self._build_random_generator()

    def _build_random_generator(self):
        # random generator
        def gen_z_prior(y):
            z_mean, z_sig = self._build_prior(y, reuse=True)
            return z_mean, z_sig

        def gen_x_sample(z):
            x_gen = self._build_generator(z, reuse=True, name='z_decoder')
            x_gen_mean = x_gen[:,:,:,:3]
            x_gen_logscale = x_gen[:,:,:,3:]
            x_gen_samples = 0.5 * (x_gen_mean + 1.0)

            x_gen_samples = tf.reshape(x_gen_samples, [1, 100*32,32,3])
            x_gen_samples = tf.split(x_gen_samples, 10, axis=1)
            x_gen_samples = tf.concat(x_gen_samples, axis=2)
            return x_gen_samples
        #
        def fix_axis(z, axis_i, axis_f):
            zsize = z.get_shape()[1].value
            mask = np.ones([100, zsize])
            mask[:, axis_i:axis_f] = 0.0
            mask = tf.constant(mask, tf.float32)
            z_masked = tf.multiply(mask, z)
            z_masked_invert = tf.expand_dims( tf.multiply(1.0-mask, z)[0, :], axis=0 )
            return z_masked + z_masked_invert

        y = gumbel_argmax(tf.ones([100, self.ysize]))
        z_mean_global, z_sig_global = gen_z_prior(y)
        z_random_global = z_mean_global + tf.random_normal(tf.shape(z_sig_global)) * z_sig_global
        z_random_local  = tf.random_normal(tf.shape(z_sig_global))
        z_random = tf.concat([z_random_global, z_random_local], axis=1)

        x_gen_samples_1 = gen_x_sample( fix_axis(z_random, 100, 200) ) # vary 0 - 100
        x_gen_samples_2 = gen_x_sample( fix_axis(z_random, 0, 100) ) # vary 100-200

        # condition on y
        def gen_x_conditioned_on_y(y_idx, local_noise, global_noise):
            y = [y_idx for j in range(100)]
            y = tf.constant(y)
            y = tf.one_hot(y, self.ysize)
            z_mean_global = gen_z_prior(y)

            z_mean_global, z_sig_global = gen_z_prior(y)
            z_random_global = z_mean_global + global_noise * z_sig_global
            z_random_local = tf.zeros([100, 100]) + local_noise #tf.random_normal([1, 100])
            z_random = tf.concat([z_random_global, z_random_local], axis=1)

            x_gen =  self._build_generator(z_random, reuse=True, name='z_decoder')
            x_gen_samples = 0.5 * (x_gen[:,:,:,:3] + 1.0)
            x_gen_samples = tf.reshape(x_gen_samples, [1, 10*10*32, 32, 3])
            x_gen_samples = tf.split(x_gen_samples, 10 , axis=1)
            x_gen_samples = tf.concat(x_gen_samples, axis=2)
            return x_gen_samples


        def gen_x_conditioned_on_zl( global_noise):
            y = [i+7 for j in range(10) for i in range(10)]
            y = tf.constant(y)
            y = tf.one_hot(y, self.ysize)
            z_mean_global = gen_z_prior(y)

            z_mean_global, z_sig_global = gen_z_prior(y)
            z_random_global = z_mean_global + global_noise * z_sig_global
            z_random_local = tf.zeros([100, 100]) + tf.random_normal([1, 100])
            z_random = tf.concat([z_random_global, z_random_local], axis=1)

            x_gen =  self._build_generator(z_random, reuse=True, name='z_decoder')
            x_gen_samples = 0.5 * (x_gen[:,:,:,:3] + 1.0)
            x_gen_samples = tf.reshape(x_gen_samples, [1, 10*10*32, 32, 3])
            x_gen_samples = tf.split(x_gen_samples, 10 , axis=1)
            x_gen_samples = tf.concat(x_gen_samples, axis=2)
            return x_gen_samples


        local_noise = tf.tile(tf.random_normal([10, 100]), [10, 1])

        global_noise = tf.reshape(
                    [tf.tile( tf.random_normal([1, 100]), [10, 1]) for i in range(10)], [100, 100])

        x_gen_condition_1 = gen_x_conditioned_on_y(1, local_noise, global_noise)

        x_gen_condition_2 = gen_x_conditioned_on_y(13, local_noise, global_noise)

        x_gen_condition_3 = gen_x_conditioned_on_y(0, local_noise, global_noise)

        x_gen_condition_4 = gen_x_conditioned_on_y(7, local_noise, global_noise)

        """
        global_noise = tf.reshape(
                    [tf.tile( tf.random_normal([1, 100]), [10, 1]) for i in range(10)], [100, 100])
        x_gen_condition_1 = gen_x_conditioned_on_zl( global_noise)

        global_noise = tf.reshape(
                    [tf.tile( tf.random_normal([1, 100]), [10, 1]) for i in range(10)], [100, 100])
        x_gen_condition_2 = gen_x_conditioned_on_zl( global_noise)

        global_noise = tf.reshape(
                    [tf.tile( tf.random_normal([1, 100]), [10, 1]) for i in range(10)], [100, 100])
        x_gen_condition_3 = gen_x_conditioned_on_zl( global_noise)

        global_noise = tf.reshape(
                    [tf.tile( tf.random_normal([1, 100]), [10, 1]) for i in range(10)], [100, 100])
        x_gen_condition_4 = gen_x_conditioned_on_zl( global_noise)

        """

        return  [x_gen_samples_1,
                 x_gen_samples_2,
                 x_gen_condition_1,
                 x_gen_condition_2,
                 x_gen_condition_3,
                 x_gen_condition_4]


    def _build_encoder(self, x, reuse=False):
        with tf.variable_scope('model'):
            with tf.variable_scope('encoder', reuse=reuse):
                xg = x[:,:,:, :3]
                xl = x[:,:,:, 3:]

                zg_mean, zg_sig, y, y_logits = self._build_encoder_g(xg)
                zl_mean, zl_sig   = self._build_encoder_l(xl)

        return zg_mean, zg_sig, zl_mean, zl_sig, y, y_logits

    def _build_encoder_g(self, xg):
        with tf.variable_scope('xg'):
            h = wn_conv(xg, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

        zg_mean, zg_sig = self._build_z_block(h, 'zg')
        y, y_logits = self._build_y_block(h, 'y')
        return zg_mean, zg_sig, y, y_logits

    def _build_encoder_l(self, xl):
        with tf.variable_scope('xl'):
            h = wn_conv(xl, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            zl_mean, zl_sig = self._build_z_block(h, 'zl')
        return zl_mean, zl_sig


    def _build_z_block(self, h, name):
        with tf.variable_scope(name):

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            z = wn_fc(h, 'fc_z', 200)
            z_mean = z[:, :100]
            z_sig  = tf.nn.softplus( z[:, 100:] )

        return z_mean, z_sig

    def _build_y_block(self, h, name):
        with tf.variable_scope(name):
            h = flatten(h)
            h = wn_fc(h, 'fc1', 500)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            h = wn_fc(h, 'fc2', 500)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.relu(h)

            y_logits = wn_fc(h, 'fc_z', self.ysize)
            y = gumbel_softmax(y_logits, tau=0.4)

        return y, y_logits

    def _build_prior(self, y, reuse=False, name='prior'):
        with tf.variable_scope('model'):
            with tf.variable_scope(name, reuse=reuse):
                h = wn_fc(y, 'fc1', 1000)
                h = tf.nn.relu(h)
                z_out = wn_fc(h, 'fc_out', 100)
                sig = tf.nn.softplus(wn_fc(h, 'fc_sig', 100))
        return z_out, sig

    def _build_generator(self, z, reuse=False, name='decoder'):
        with tf.variable_scope('model'):
            with tf.variable_scope(name, reuse=reuse):

                h = wn_fc(z, 'h1', 200)
                h = tf.reshape(h, [-1, 1, 1, 200])
                h = tf.image.resize_images(h, [4,4], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.relu(h)

                h = wn_conv(h, 'conv1', nf=128, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [8,8], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.relu(h)

                h = wn_conv(h, 'conv2', nf=64, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [16,16], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.relu(h)

                h = wn_conv(h, 'conv3', nf=32, rf=6, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [32,32], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.relu(h)

                x = wn_conv(h, 'conv4', nf=6, rf=6, stride=1, pad='SAME')

        return x

    def train(self, summary=True):
        if summary:
            return self.sess.run([self.summary, self.train_op])
        else:
            return self.sess.run([self.train_op])

    def random_generation(self):
        return self.sess.run(self.random_gen_summary)

    def _build_accuracy(self, py, labels):
        # for each cluster
        # find an example that have highest py and assign the cluster
        # with true label from that example.
        idx = tf.argmax(py, axis=0)
        pred = tf.argmax(py, axis=1)
        labels = tf.argmax(labels, axis=1)
        # create a mapping
        examples = tf.gather(idx, pred)
        new_pred = tf.gather(labels, examples)
        acc = tf.cast( tf.equal(new_pred,labels) , tf.float32)
        return tf.reduce_mean(acc)

if __name__ == "__main__":

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:

        data = SVHN_AUG(sess, train_batch_size=10)

        x = data.next_train_images
        labels = data.next_train_labels

        sampler = VAE(sess, x, labels, 30, 10)
        saver = tf.train.Saver()
        saver.restore(sess, 'models/v5_beta20_alpha1_ysize30/model.ckpt')


        """
        # Figure 1b
        figure1b = np.ones([32*4 + 5, 32*10, 3])
        img, img_recon, img_shuffle_recon = sess.run([sampler.x, sampler.x_recon, sampler.xw_recon])

        img_normal  = ( img[:,:,:,:3] + 1.0 ) * 0.5
        img_shuffle = ( img[:,:,:,3:] + 1.0 ) * 0.5

        img_recon    =  np.clip( ( img_recon[:,:,:,:3] + 1.0 ) * 0.5, 0.0 ,1.0)
        img_shuffle_recon =  np.clip( ( img_shuffle_recon[:,:,:,:3] + 1.0 ) * 0.5, 0.0, 1.0 )
        for i in range(10):
            figure1b[:32, 32*i: 32*(i+1), :] = img_normal[i]
            figure1b[32:32*2, 32*i: 32*(i+1), :] = img_shuffle[i]
            figure1b[32*2+5:32*3+5, 32*i: 32*(i+1), :] = img_recon[i]
            figure1b[32*3+5:, 32*i: 32*(i+1), :] = img_shuffle_recon[i]


        figure1b = (figure1b * 255.).astype(np.uint8)
        im = Image.fromarray(figure1b)
        im.save("x_and_recon.jpeg")

        """
        """
        # Figure 2
        img1, img2, gen1, gen2, gen3, gen4 = sess.run(sampler.random_images)

        def save(img, name):
            img = np.clip(img, 0, 1.0)
            img = (img[0] * 255.).astype(np.uint8)
            im = Image.fromarray(img)
            im.save(name+".jpeg")

        save(gen1, 'gen1')
        save(gen2, 'gen2')
        save(gen3, 'gen3')
        save(gen4, 'gen4')

        """

        ## Let's try segmentation !
