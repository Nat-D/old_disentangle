# TODO: attach linear classifier on the latent variable and evaluate
# TODO: evaluate classifier on generated samples

import tensorflow as tf
import numpy as np

from data import *
from utils import discretised_logistic_loss, \
                  samples_logistic_distribution,\
                  ortho_init, \
                  gaussian_log_likelihood, fc, flatten,\
                  gumbel_softmax, gumbel_argmax

def wn_fc(x, scope, nh, *, init_scale=1.0, init_bias=0.0, trainable=True):
    with tf.variable_scope(scope):
        nin = x.get_shape()[1].value
        w = tf.get_variable("w", [nin, nh], initializer=ortho_init(init_scale), trainable=trainable)
        b = tf.get_variable("b", [nh], initializer=tf.constant_initializer(init_bias), trainable=trainable)
        return tf.matmul(x, w)+b

def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)
    return _initializer

def wn_conv(x, scope, nf, rf, stride, pad="SAME", init_scale=1.0, dtype=tf.float32, collections=None):
    with tf.variable_scope(scope):
        stride_shape = [1, stride, stride, 1]
        filter_shape = [rf, rf, int(x.get_shape()[3]), nf]

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = np.prod(filter_shape[:3])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = np.prod(filter_shape[:2]) * nf
        # initialize weights with random weights
        w_bound = np.sqrt(6. / (fan_in + fan_out))

        w = tf.get_variable("W", filter_shape, dtype, tf.random_uniform_initializer(-w_bound, w_bound),
                            collections=collections)
        b = tf.get_variable("b", [1, 1, 1, nf], initializer=tf.constant_initializer(0.0),
                            collections=collections)

        return tf.nn.conv2d(x, w, stride_shape, pad) + b


class VAE:
    def __init__(self, sess, x, labels, ysize, beta, alpha, tau,
                 reuse=False, training=True, zl_random=False, zg_random=False):
        self.sess = sess
        self.tau = tau
        self.ysize = ysize

        xz = x[:,:,:, :3]
        xw = x[:,:,:, 3:]

        # build autoencoder
        zg_mean, zg_sig, zl_mean, zl_sig  = self._build_encoder(x, reuse=reuse)

        if zl_random:
            zl_mean = zl_mean * 0.0
            zl_sig  = zl_sig * 0.0 + 1.0

        if zg_random:
            zg_mean = zg_mean * 0.0
            zg_sig = zg_sig * 0.0 + 1.0


        z_mean = tf.concat([zg_mean, zl_mean], axis=1)
        z_sig  = tf.concat([zg_sig, zl_sig], axis=1)
        eps = tf.random_normal(tf.shape(z_sig))
        self.z_sample = z_sample = z_mean + eps * z_sig
        self.zsize = z_sample.get_shape()[1].value

        x_recon = self._build_generator(z_sample, reuse=reuse, name='z_decoder')
        x_mean = x_recon[:,:,:,:3]
        x_logscale = x_recon[:,:,:,3:]
        self.x_outs = samples_logistic_distribution(x_mean, x_logscale)

        kl_loss = tf.reduce_mean(
                    - gaussian_log_likelihood(z_sample, 0.0, 1.0) \
                    + gaussian_log_likelihood(z_sample, z_mean, z_sig)
                    )

        # recon loss function
        recon_loss =tf.reduce_mean(
                    tf.reduce_sum(
                    discretised_logistic_loss(xz, x_mean, x_logscale),axis=[1,2,3]))


        z_sample_g, z_sample_l = tf.split(z_sample, 2, axis=1)
        # Auxiliary model
        xw_recon = self._build_generator(z_sample_l, reuse=reuse, name='w_decoder')
        xw_mean = xw_recon[:,:,:,:3]
        xw_logscale = xw_recon[:,:,:,3:]

        recon_loss_w =tf.reduce_mean(
                    tf.reduce_sum(
                    discretised_logistic_loss(xw, xw_mean, xw_logscale),axis=[1,2,3]))


        # optimise  beta, alpha
        total_loss = (recon_loss) + (recon_loss_w ) + (beta * 1.0) * kl_loss

        if training:
            optimizer  = tf.train.AdamOptimizer(0.0001)
            self.train_op = optimizer.minimize(total_loss)


        """
        SUMMARY
        """
        self.summary = tf.summary.merge([
            tf.summary.scalar("train/recon_loss", recon_loss),
            tf.summary.scalar("train/kl_loss", kl_loss),
            tf.summary.scalar("train/recon_loss_w", recon_loss_w),
            tf.summary.scalar("train/total_loss", total_loss),
            tf.summary.image("x/x", 0.5*(xz+1.)),
            tf.summary.image("w/x_shuffle", 0.5*(xw+1.)),
            tf.summary.image("x/recon", 0.5*(x_mean[:,:,:,:3] + 1.)),
            tf.summary.image("w/recon", 0.5*(xw_mean[:,:,:,:3] + 1.)),
            ])

        self.vis_placeholder = tf.placeholder(tf.uint8)
        self.random_gen_summary = self._build_random_generator()

    def _build_random_generator(self):
        # random generator

        def gen_x_sample(z):
            x_gen = self._build_generator(z, reuse=True, name='z_decoder')
            x_gen_mean = x_gen[:,:,:,:3]
            x_gen_logscale = x_gen[:,:,:,3:]
            x_gen_samples = 0.5*x_gen_mean +1.0

            x_gen_samples = tf.reshape(x_gen_samples, [1, 100*32,32,3])
            x_gen_samples = tf.split(x_gen_samples, 10, axis=1)
            x_gen_samples = tf.concat(x_gen_samples, axis=2)
            return x_gen_samples
        #
        def fix_axis(z, axis_i, axis_f):
            zsize = z.get_shape()[1].value
            mask = np.ones([100, zsize])
            mask[:, axis_i:axis_f] = 0.0
            mask = tf.constant(mask, tf.float32)
            z_masked = tf.multiply(mask, z)
            z_masked_invert = tf.expand_dims( tf.multiply(1.0-mask, z)[0, :], axis=0 )
            return z_masked + z_masked_invert

        z_random = tf.random_normal([100, self.zsize])

        x_gen_samples_1 = gen_x_sample( fix_axis(z_random, 100, 200) ) # vary 0 - 100
        x_gen_samples_2 = gen_x_sample( fix_axis(z_random, 0, 100) ) # vary 100-200
        return tf.summary.merge(
                    [tf.summary.image('random_gen/z1', x_gen_samples_1),
                     tf.summary.image('random_gen/z2', x_gen_samples_2)]
                    )


    def _build_encoder(self, x, reuse):
        #with tf.variable_scope('model', reuse=reuse):
            #with tf.variable_scope('encoder', reuse=reuse):
        xg = x[:,:,:, :3]
        xl = x[:,:,:, 3:]

        zg_mean, zg_sig   = self._build_encoder_g(xg, reuse)
        zl_mean, zl_sig   = self._build_encoder_l(xl, reuse)

        return zg_mean, zg_sig, zl_mean, zl_sig

    def _build_encoder_g(self, xg, reuse):
        with tf.variable_scope('xg', reuse=reuse):
            h = wn_conv(xg, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            zg_mean, zg_sig = self._build_z_block(h, 'zg', reuse=reuse)

        return zg_mean, zg_sig

    def _build_encoder_l(self, xl, reuse):
        with tf.variable_scope('xl', reuse=reuse):
            h = wn_conv(xl, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            zl_mean, zl_sig = self._build_z_block(h, 'zl', reuse=reuse)
        return zl_mean, zl_sig


    def _build_z_block(self, h, name, reuse):
        with tf.variable_scope(name, reuse=reuse):

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            z_mean = wn_fc(h, 'fc_z', 100)
            #z_sig  = tf.nn.sigmoid( wn_fc(h, 'fc_z_sig', 100, init_scale=1.0))
            z_sig  = tf.nn.softplus( wn_fc(h, 'fc_z_sig', 100, init_scale=1.0))

        return z_mean, z_sig


    def _build_generator(self, z, reuse, name='decoder'):
        with tf.variable_scope('model'):
            with tf.variable_scope(name, reuse=reuse):

                h = wn_fc(z, 'h1', 200)
                h = tf.reshape(h, [-1, 1, 1, 200])
                h = tf.image.resize_images(h, [4,4], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv1', nf=128, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [8,8], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv2', nf=64, rf=4, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [16,16], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                h = wn_conv(h, 'conv3', nf=32, rf=6, stride=1, pad='SAME')
                h = tf.image.resize_images(h, [32,32], align_corners=True)
                h = tf.nn.dropout(h, keep_prob=0.8)
                h = tf.nn.elu(h)

                x = wn_conv(h, 'conv4', nf=6, rf=6, stride=1, pad='SAME')

        return x

    def train(self, summary=True):
        if summary:
            return self.sess.run([self.summary, self.train_op])
        else:
            return self.sess.run([self.train_op])

    def random_generation(self):
        return self.sess.run(self.random_gen_summary)

class Classifier:
    def __init__(self, sess, x, reuse=False):
        self.sess = sess

        self.y_pred = self._build_classifier(x, reuse=reuse)

    def _build_classifier(self, x, reuse):
        with tf.variable_scope('classifier', reuse=reuse):
            h = wn_conv(x, 'conv1', nf=32, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv2', nf=64, rf=6, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv3', nf=128, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_conv(h, 'conv4', nf=248, rf=4, stride=2, pad='SAME')
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = flatten(h)
            h = wn_fc(h, 'fc1', 1000)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            h = wn_fc(h, 'fc2', 200)
            h = tf.nn.dropout(h, keep_prob=0.8)
            h = tf.nn.elu(h)

            y_logits = wn_fc(h, 'y_logits', 10)
            y_pred = tf.argmax(y_logits, axis=1)

        return y_pred


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--logdir', help='tb directory', default='experiments_class/')
    parser.add_argument('--modeldir',help='model directory', default='models/')
    parser.add_argument('--exp_name', default='test')
    parser.add_argument('--gpu', default="0")
    parser.add_argument('--bs', default="256", type=int)
    parser.add_argument('--ysize', default="30", type=int)
    parser.add_argument('--beta', default="20.0", type=float)
    parser.add_argument('--alpha', default="1.0", type=float)
    parser.add_argument('--tau', default="0.4", type=float)

    args = parser.parse_args()

    summary_dir = args.logdir + args.exp_name
    model_dir = args.modeldir + args.exp_name

    import os
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
        os.makedirs(model_dir + '/at_1m')
        os.makedirs(model_dir + '/at_2m')
        os.makedirs(model_dir + '/at_3m')
        os.makedirs(model_dir + '/at_4m')

    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"]=args.gpu


    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    with tf.Session(config=config) as sess:
        summary_writer = tf.summary.FileWriter(summary_dir)

        data = SVHN_AUG(sess, train_batch_size=args.bs, test_batch_size=1000)

        x = data.next_train_images
        labels = data.next_train_labels
        net = VAE(sess, x, labels, ysize=args.ysize, beta=args.beta,
                  alpha=args.alpha, tau=args.tau)
        sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()


        # eval graph
        x_test = data.next_test_images
        x_labels = data.next_test_labels

        eval_net = VAE(sess, x_test, x_labels, ysize=args.ysize, beta=args.beta,
                  alpha=args.alpha, tau=args.tau, reuse=True, training=False)

        classifier = Classifier(sess, eval_net.x_outs)
        saver2 = tf.train.Saver([v for v in tf.global_variables() if 'classifier' in v.name])
        saver2.restore(sess, 'models/classifier_3/model.ckpt')

        eval_acc = tf.reduce_sum(
                    tf.cast( tf.equal(classifier.y_pred,  tf.argmax(x_labels, axis=1)) , tf.float32) )

        # eval with random zl
        eval_net_random = VAE(sess, x_test, x_labels, ysize=args.ysize, beta=args.beta,
                  alpha=args.alpha, tau=args.tau, reuse=True, training=False, zl_random=True)
        classifier_2 =  Classifier(sess, eval_net_random.x_outs, reuse=True)
        eval_acc_random = tf.reduce_sum(
                    tf.cast( tf.equal(classifier_2.y_pred,  tf.argmax(x_labels, axis=1)) , tf.float32) )

        # eval with random zg
        eval_net_random_zg = VAE(sess, x_test, x_labels, ysize=args.ysize, beta=args.beta,
                  alpha=args.alpha, tau=args.tau, reuse=True, training=False, zg_random=True)
        classifier_3 =  Classifier(sess, eval_net_random_zg.x_outs, reuse=True)
        eval_acc_random_zg = tf.reduce_sum(
                    tf.cast( tf.equal(classifier_3.y_pred,  tf.argmax(x_labels, axis=1)) , tf.float32) )

        for i in range(5000002):

            if (i) % 5000 == 0:
                summary, _ = net.train()
                random_gen_summary = net.random_generation()

                if i % 20000 == 0:
                    #eval_sum = sess.run(eval_summary)
                    eval_sum_zg, eval_sum_zl, eval_sum = 0, 0, 0
                    for j in range(20): #eval total of 20000 points
                        _eval_sum, _eval_sum_zl, _eval_sum_zg = sess.run([eval_acc,
                                                               eval_acc_random,
                                                               eval_acc_random_zg])
                        eval_sum    += _eval_sum
                        eval_sum_zg += _eval_sum_zg
                        eval_sum_zl += _eval_sum_zl

                    eval_sum    = eval_sum/20000.
                    eval_sum_zg = eval_sum_zg/20000.
                    eval_sum_zl = eval_sum_zl/20000.

                    eval_sum = tf.Summary(value=[
                                tf.Summary.Value(tag="train/eval_acc", simple_value=eval_sum),
                                tf.Summary.Value(tag="train/eval_acc_random_zl", simple_value=eval_sum_zl),
                                tf.Summary.Value(tag="train/eval_acc_random_zg", simple_value=eval_sum_zg),
                               ])

                    summary_writer.add_summary(eval_sum, i)

                summary_writer.add_summary(summary, i)
                summary_writer.add_summary(random_gen_summary, i)
                summary_writer.flush()
                saver.save(sess, model_dir + '/model.ckpt')

            else:
                _ = net.train(False)


            if  i == 1000000:
                saver.save(sess, model_dir + '/at_1m/model.ckpt')
            if  i == 2000000:
                saver.save(sess, model_dir + '/at_2m/model.ckpt')
            if  i == 3000000:
                saver.save(sess, model_dir + '/at_3m/model.ckpt')
            if  i == 4000000:
                saver.save(sess, model_dir + '/at_4m/model.ckpt')
