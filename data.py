import numpy as np
import tensorflow as tf
import scipy.io as sio

DIR = '/media/nat/Disk1/Data/'


class TextureMNIST:
    def __init__(self, sess,  train_batch_size):
        # load and preprocess data
        filenames = [DIR+"texturedMNIST/train.tfrecords",
                    ]
        dataset = tf.data.TFRecordDataset(filenames)


        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)

          image = tf.reshape(image, [28, 28, 2])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=20000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels


class MNIST:
    def __init__(self, sess, train_batch_size, test_batch_size=None):
        # load and preprocess data
        filenames = [DIR+"MNIST/mnist_train.tfrecords"]
        dataset = tf.data.TFRecordDataset(filenames)

        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [28, 28, 1])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0 # scale to -1 to 1

          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=10000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = [DIR+"/mnist_test.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.shuffle(buffer_size=10000)
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels


class CIFAR10:
    def __init__(self, sess, train_batch_size, test_batch_size=None):
        # load and preprocess data
        filenames = [DIR+"cifar10/data_batch_1.tfrecords",
                     DIR+"cifar10/data_batch_2.tfrecords",
                     DIR+"cifar10/data_batch_3.tfrecords",
                     DIR+"cifar10/data_batch_4.tfrecords",
                     DIR+"cifar10/data_batch_5.tfrecords"]

        dataset = tf.data.TFRecordDataset(filenames)

        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [3, 32, 32])
          image = tf.transpose(image, [1, 2, 0]) # [c, w,h] -> [w, h, c]
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0 # scale to -1 to 1
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=10000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = [DIR+"/test_batch.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.shuffle(buffer_size=10000)
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels

class CIFAR10_AUG:
    def __init__(self, sess, train_batch_size, test_batch_size=None):
        # load and preprocess data
        filenames = [DIR+"cifar10_aug/data_batch_1.tfrecords",
                     DIR+"cifar10_aug/data_batch_2.tfrecords",
                     DIR+"cifar10_aug/data_batch_3.tfrecords",
                     DIR+"cifar10_aug/data_batch_4.tfrecords",
                     DIR+"cifar10_aug/data_batch_5.tfrecords"]

        dataset = tf.data.TFRecordDataset(filenames)

        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [6, 32, 32])
          image = tf.transpose(image, [1, 2, 0]) # [c, w,h] -> [w, h, c]
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0 # scale to -1 to 1
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=10000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = [DIR+"/test_batch.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.shuffle(buffer_size=10000)
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels


class SVHN:
    def __init__(self, sess,  train_batch_size, test_batch_size=None):
        # load and preprocess data
        filenames = [DIR+"SVHN/train.tfrecords",
                     DIR+"SVHN/extra.tfrecords",
                    ]
        dataset = tf.data.TFRecordDataset(filenames)


        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [32, 32, 3])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=20000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = [DIR+"SVHN/test.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels


class SVHN_AUG:
    def __init__(self, sess,  train_batch_size, test_batch_size=None):
        # load and preprocess data

        filenames = [DIR+"SVHN_AUG/train.tfrecords",
                     DIR+"SVHN_AUG/extra.tfrecords",
                     DIR+"SVHN_AUG_2/train.tfrecords",
                     DIR+"SVHN_AUG_2/extra.tfrecords",
                     DIR+"SVHN_AUG_3/train.tfrecords",
                     DIR+"SVHN_AUG_3/extra.tfrecords",
                    ]

        dataset = tf.data.TFRecordDataset(filenames)


        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [32, 32, 6])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=20000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels

        if test_batch_size is not None:
            test_filename = [DIR+"SVHN_AUG/test.tfrecords"]
            test_dataset = tf.data.TFRecordDataset(test_filename)
            test_dataset = test_dataset.map(_parse_function)
            test_dataset = test_dataset.repeat()
            test_dataset = test_dataset.batch(test_batch_size)
            test_iterator = test_dataset.make_one_shot_iterator()
            next_test_images, next_test_labels = test_iterator.get_next()
            self.next_test_images = next_test_images
            self.next_test_labels = next_test_labels


class SVHN_DISTRACTOR:
    def __init__(self, sess,  train_batch_size):
        # load and preprocess data

        filenames = [DIR+"SVHN_distractor/train.tfrecords",
                     DIR+"SVHN_distractor/extra.tfrecords",
                    ]

        dataset = tf.data.TFRecordDataset(filenames)


        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                      "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]
          label = parsed_features["label"]
          label = tf.one_hot(label, 10)

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [32, 32, 6])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0
          return image, label

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=20000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images, next_labels = iterator.get_next()
        self.next_train_images = next_images
        self.next_train_labels = next_labels


class STL10:
    def __init__(self, sess,  train_batch_size):
        # load and preprocess data
        filenames = [DIR+"stl10_matlab/train.tfrecords",
                     DIR+"stl10_matlab/train_extra.tfrecords"]
        dataset = tf.data.TFRecordDataset(filenames)


        def _parse_function(example_proto):
          features = {"image": tf.FixedLenFeature((), tf.string, default_value="")
                     }
          parsed_features = tf.parse_single_example(example_proto, features)
          image_raw = parsed_features["image"]

          image = tf.decode_raw(image_raw, tf.uint8)
          image = tf.reshape(image, [96, 96, 3])
          image = tf.cast(image, tf.float32)/255.
          image = 2.0 * image - 1.0
          return image

        dataset = dataset.map(_parse_function)
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=20000)
        dataset = dataset.batch(train_batch_size)

        iterator = dataset.make_one_shot_iterator()
        next_images = iterator.get_next()
        self.next_train_images = next_images


def read_data(path):
    images_raw = sio.loadmat(path)
    images = images_raw['X']
    labels = images_raw['y']
    return images, labels

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def save_to_tfrecord(train_filename, images, labels):
    # open the TFRecords file
    writer = tf.python_io.TFRecordWriter(train_filename)
    for index in range(len(labels)):
        image_raw = images[index]
        image_raw = image_raw.tostring()
        # Create a feature
        feature = {'label': _int64_feature(int(labels[index])),
                   'image': _bytes_feature(image_raw)}

        # Create an example protocol buffer
        example = tf.train.Example(features=tf.train.Features(feature=feature))

        # Serialize to string and write on the file
        writer.write(example.SerializeToString())
    writer.close()

def scamble_image(imgs):
    imgs_scamble = np.array(imgs)
    for idx, img in enumerate(imgs):
        # let's use patch of size 8x8 SVHN_AUG
        i_mix = [i for i in range(4)]
        j_mix = [j for j in range(4)]
        np.random.shuffle(i_mix)
        np.random.shuffle(j_mix)
        for i in range(4):
            for j in range(4):
                imgs_scamble[idx, 8*i:8*(i+1), 8*j:8*(j+1), :] = \
                    img[8*i_mix[i]:8*(i_mix[i]+1), 8*j_mix[j]:8*(j_mix[j]+1), :]

    return np.concatenate( (imgs, imgs_scamble), axis=3 )

def scamble_image_middle(imgs):
    imgs_scamble = np.array(imgs)
    for idx, img in enumerate(imgs):
        # let's use patch of size 8x8 SVHN_AUG
        i_mix = [i for i in range(4)]
        j_mix = [j for j in range(4)]

        np.random.shuffle(i_mix)
        np.random.shuffle(j_mix)
        for i in range(4):
            for j in range(4):
                imgs_scamble[idx, 8*i:8*(i+1), 8*j:8*(j+1), :] = \
                    img[8*i_mix[i]:8*(i_mix[i]+1), 8*j_mix[j]:8*(j_mix[j]+1), :]

        # reset the side back to normal
        imgs_scamble[idx, :, :10,:] = img[:, :10, :]
        imgs_scamble[idx, :, 22:,:] = img[:, 22:, :]

    return np.concatenate( (imgs, imgs_scamble), axis=3 )


if __name__ == "__main__":

    # (32, 32, 3, 73257), (73257, 1)
    train_images, train_labels = read_data(DIR+'SVHN/train_32x32.mat')
    train_images = np.transpose(train_images, (3, 0, 1, 2))
    train_images = scamble_image_middle(train_images)
    train_labels[train_labels == 10] = 0
    train_labels = np.squeeze(train_labels)
    save_to_tfrecord(DIR + 'SVHN_distractor/train.tfrecords', train_images, train_labels)

    #(32, 32, 3, 26032), (26032, 1)
    test_images, test_labels = read_data(DIR+'SVHN/test_32x32.mat')
    test_images = np.transpose(test_images, (3, 0, 1, 2))
    test_images = scamble_image_middle(test_images)
    test_labels[test_labels == 10] = 0
    test_labels = np.squeeze(test_labels)
    save_to_tfrecord(DIR + 'SVHN_distractor/test.tfrecords', test_images, test_labels)

    # extra
    test_images, test_labels = read_data(DIR+'SVHN/extra_32x32.mat')
    test_images = np.transpose(test_images, (3, 0, 1, 2))
    test_images = scamble_image_middle(test_images)
    test_labels[test_labels == 10] = 0
    test_labels = np.squeeze(test_labels)
    save_to_tfrecord(DIR + 'SVHN_distractor/extra.tfrecords', test_images, test_labels)

    print('done')
    """
    with tf.Session() as sess:
        stl = STL10(sess, 10)
        imgs = sess.run(stl.next_train_images)
        print(np.shape(imgs))
        import matplotlib.pyplot as plt
        plt.imshow(imgs[0])
        plt.show()
    """
